using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ChoiceUIController : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("GameObject/UI/GM/Choice Based UI Panel")]
    public static void AddChoiceBasedUIPanel()
    {
        GameObject obj = Instantiate(Resources.Load<GameObject>("ChoicePanelHolder"));
        obj.transform.SetParent(Selection.activeGameObject.transform,false);
        
        //Create A scriatable and set here while creating this -- update things later
    }
#endif
    public bool isShow;
    public GameObject choiceHolder;
    
    public ChoiceItemScriptable choiceItemData;
    
    [Range(0, 5)] public int itemNumber;
    public GameObject itemPrefab;
    public Transform itemParentTrans;
    public List<ChoiceItem> choiceItemList;

    public int currentSelectedItemId;
    
    
    public UnityAction<int> OnSendRegisteredListener;
    
    
    private void Awake()
    {
       Init();
    }

    private void Init()
    {
        for (int i = 0; i < itemNumber; i++)
        {
            GameObject go = Instantiate(itemPrefab);
            go.transform.SetParent(itemParentTrans);

            ChoiceItem choiceItem = go.GetComponent<ChoiceItem>();
            choiceItem.InitWithData(choiceItemData.choiceItemDataList[i]);
            choiceItemList.Add(choiceItem);
        }

        ChoiceItem.OnButtonEvent += ChoiceButtonEventPressed;
    }

    private void ChoiceButtonEventPressed(int itemID)
    {
        Debug.Log("======== ITEM Selected ID======"+itemID);
        currentSelectedItemId = itemID;
        OnSendRegisteredListener?.Invoke(itemID);
        
        //create this generic, for now hardcoding
        switch (itemID)
        {
            case 0:
               
                break;
            case 1:
               
                break;
            case 2:
                
                break;
            default:
                break;
        }
        Deactivate();
    }

    
    
    public void Activate()
    {
        choiceHolder.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        choiceHolder.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        ChoiceItem.OnButtonEvent -= ChoiceButtonEventPressed;
    }
}
