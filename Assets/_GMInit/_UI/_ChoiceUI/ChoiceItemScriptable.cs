using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "ChoiceItemData", menuName = "ScriptableObjects/Choice Item Data", order = 1)]
public class ChoiceItemScriptable : ScriptableObject
{
    public List<ChoiceItemStruct> choiceItemDataList;
}

[System.Serializable]
public struct ChoiceItemStruct
{
    public string name;
    public int id;
    public Sprite image;
    public string description;
}