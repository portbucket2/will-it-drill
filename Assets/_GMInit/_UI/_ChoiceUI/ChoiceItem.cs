using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceItem : MonoBehaviour
{
    public Button button;
    public Image image;
    public TextMeshProUGUI nameText;

    public ChoiceItemStruct choiceData;
    
    public static event Action<int> OnButtonEvent;
    private void OnEnable()
    {
        Init();
    }

    public void Init()
    {
        button = GetComponent<Button>();
        image = transform.GetChild(0).GetComponent<Image>();
        nameText = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
    }

    public void InitWithData(ChoiceItemStruct choiceItem)
    {
        this.choiceData = choiceItem;
        button.onClick.RemoveAllListeners();
        
        if (image != null) image.sprite = choiceItem.image;
        if (nameText != null) nameText.text = choiceItem.name;
        if(button != null) button.onClick.AddListener(delegate { OnButtonEvent?.Invoke(choiceData.id); });
    }
}
