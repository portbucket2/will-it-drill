using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class ProgressBar : MonoBehaviour
{
   #if UNITY_EDITOR
   [MenuItem("GameObject/UI/GM/Linear Progress Bar")]
   public static void AddLinearProgressBar()
   {
      GameObject obj = Instantiate(Resources.Load<GameObject>("LinearProgressBarHolder"));
      obj.transform.SetParent(Selection.activeGameObject.transform,false);
   }
   
   [MenuItem("GameObject/UI/GM/Radial Progress Bar")]
   public static void AddRadialProgressBar()
   {
      GameObject obj = Instantiate(Resources.Load<GameObject>("RoundProgressBarHolder"));
      obj.transform.SetParent(Selection.activeGameObject.transform,false);
   }
   #endif
   
   
   [Header("Fill Bar Behaviour")]
   [Range(0,1000)]
   public float minimum;
   
   [Range(0,1000)]
   public float maximum;

   public int currentValue;
   
   [Range(0.01f,5f)]
   public float fillTime;
   
   
   [Header("References")]
   public Image fillerImage;

   [Header("Style")] 
   public Image.Type imageType = Image.Type.Filled;
   public Image.FillMethod fillMethod = Image.FillMethod.Horizontal;
   public int fillOrigin = 0;
  

   private Action OnCompleteFill;
   private bool m_IsInstantFill;
   
   private void Start()
   {
      fillerImage.type = Image.Type.Filled;
      fillerImage.fillMethod = fillMethod;
      fillerImage.fillOrigin = fillOrigin;
      
     // ----------Test Function Call----------
      FillProgressBar(0,100,75, 2f,false,() =>
      {
         Debug.Log("Fill Complete!");
      });
   }

   public void FillProgressBar(float t_Minimum, float t_Maximum, float t_CurrentValue, float t_FillTime, bool t_IsInstantFill = false, Action OnCompleteFill = null)
   {
      this.OnCompleteFill = OnCompleteFill;
      fillTime = t_FillTime;
      
      float t_currentOffset = t_CurrentValue - t_Minimum;
      float t_MaximumOffset = t_Maximum - t_Minimum;
      float t_FillAmount = t_currentOffset / t_MaximumOffset;
      float t_CurrentFillAtUI = fillerImage.fillAmount;
      
      StartCoroutine(FillBarRoutine(t_CurrentFillAtUI,t_FillAmount));
   }
   
   private IEnumerator FillBarRoutine(float t_Current,float t_Destination)
   {
      float t_Progression = 0f;
      float t_Duration = m_IsInstantFill? 0.01f : fillTime;
      float t_EndTIme = Time.time + t_Duration;
      float t_CurrentFillValue = t_Current;
      float t_DestValue = t_Destination;
      
      
      WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
      while (true)
      {
         t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
         
         fillerImage.fillAmount = Mathf.Lerp(t_CurrentFillValue,t_DestValue,t_Progression);
        
         if (t_Progression >= 1f)
         {
            OnCompleteFill?.Invoke();
            break;
         }
 
         yield return t_CycleDelay;
      }
      
      StopCoroutine(FillBarRoutine(0,0));
   }

}
