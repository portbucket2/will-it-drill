using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.gm.modalwindow
{
    public class AnimEvent : MonoBehaviour
    {
        public UnityEvent OnAnimEntryDoneEvent;
        public UnityEvent OnAnimExitDoneEvent;
        
        public void EntryDoneCallEvent()
        {
            OnAnimEntryDoneEvent?.Invoke();
        }

        public void ExitDoneCallEvent()
        {
            OnAnimExitDoneEvent?.Invoke();
        }
    }
}

