using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ModalWindowController : MonoBehaviour
{
#if UNITY_EDITOR
   [MenuItem("GameObject/UI/GM/Modal Popup Base")]
   public static void AddModalPopup()
   {
      GameObject obj = Instantiate(Resources.Load<GameObject>("Modal Window Holder"));
      obj.transform.SetParent(Selection.activeGameObject.transform,false);
   }
#endif
   
   public GameObject modalWindowObject;

   [Header("Header Part")] 
   [SerializeField] private GameObject headerArea;
   [SerializeField] private TextMeshProUGUI headerText;
   
   [Header("Content Part")] 
   [SerializeField] private GameObject contentArea;
   [SerializeField] private Image contentImage;
   [SerializeField] private TextMeshProUGUI contentText;
   
   [Header("Footer Part")] 
   [SerializeField] private GameObject footerArea;
   [SerializeField] private Button acceptButton;
   [SerializeField] private Button declineButton;
  

   private Action OnAcceptAction;
   private Action OnDeclineAction;

   [Header("Animations")] public Animator modalAnimator;
   
   private int ENTRY_ANIM = Animator.StringToHash("entry");
   private int EXIT_ANIM = Animator.StringToHash("exit");


   private void Activate()
   {
      modalWindowObject.SetActive(true);
   }

   private void Deactivate()
   {
      modalWindowObject.SetActive(false);
   }

   public void ShowModalWindow(string title,Sprite imageToShow,string message,Action confirmAction,Action declineAction)
   {
      Activate();
      
      //Hide header if there's no title
      bool hasTitle = string.IsNullOrEmpty(title);
      headerArea.SetActive(hasTitle);
      headerText.text = title;
      
      //Content Area
      bool isImage = (imageToShow != null) ? true : false;
      bool isMessage = string.IsNullOrEmpty(message);

      bool isContentArea = (isImage || isMessage) ? true : false;
      contentArea.SetActive(isContentArea);
      
      contentImage.gameObject.SetActive(isImage);
      contentImage.sprite = imageToShow;
      
      contentText.gameObject.SetActive(isMessage);
      contentText.text = message;
      
      //Footer Area
      OnAcceptAction = confirmAction;
      OnDeclineAction = declineAction;
      
   }

   private void PlayEntry()
   {
      modalAnimator.SetTrigger(ENTRY_ANIM);
   }
   private void PlayExit()
   {
      modalAnimator.SetTrigger(EXIT_ANIM);
   }

   public void Close()
   {
      throw new NotImplementedException();
   }
   
   
   public void Confirm()
   {
      OnAcceptAction?.Invoke();
      Close();
   }

   public void Decline()
   {
      OnDeclineAction?.Invoke();
      Close();
   }

   public void ModalWindowEntryDone()
   {
      Debug.Log("Modal Window ENTRY Done Call Event! -------- ");
   }

   public void ModalWindowExitDone()
   {
      Debug.Log("Modal Window EXIT Done Call Event! -------- ");
   }
}
