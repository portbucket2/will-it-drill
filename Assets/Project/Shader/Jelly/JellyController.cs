using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ShadValue
{
    public float height;
    public float speed;
    public float distortion;
    public float scale;
}

public class JellyController : MonoBehaviour
{
    public string JELLY_HEIGHT ="Jelly_Height";
    public string WOBBLE_SPEED ="WobbleSpeed";
    public string DISTORTION ="Distortion";
    public string DISTORTION_SCALE = "DistortionScale";

    public Material jellyMat;

    public ShadValue initValue;

    public ShadValue destValue;


    private void Start()
    {
        jellyMat.SetFloat(JELLY_HEIGHT,initValue.height);
        jellyMat.SetFloat(WOBBLE_SPEED,initValue.speed);
        jellyMat.SetFloat(DISTORTION,initValue.distortion);
        jellyMat.SetFloat(DISTORTION_SCALE,initValue.scale);
    }

    public void BeaconStart()
    {
        StartCoroutine(JellyHeightRoutine(destValue.height));
        StartCoroutine(WobbleSpeedRoutine(destValue.speed));
        StartCoroutine(DistortionRoutine(destValue.distortion));
        StartCoroutine(DistortionScaleRoutine(destValue.scale));
    }
    public void BeaconExit()
    {
        StartCoroutine(JellyHeightRoutine(initValue.height));
        StartCoroutine(WobbleSpeedRoutine(initValue.speed));
        StartCoroutine(DistortionRoutine(initValue.distortion));
        StartCoroutine(DistortionScaleRoutine(initValue.scale));
    }
    public void BananaStart()
    {
        StartCoroutine(JellyHeightRoutine(destValue.height));
        StartCoroutine(WobbleSpeedRoutine(destValue.speed));
        StartCoroutine(DistortionRoutine(destValue.distortion));
        StartCoroutine(DistortionScaleRoutine(destValue.scale));
    }
    public void BananaExit()
    {
        StartCoroutine(JellyHeightRoutine(initValue.height));
        StartCoroutine(WobbleSpeedRoutine(initValue.speed));
        StartCoroutine(DistortionRoutine(initValue.distortion));
        StartCoroutine(DistortionScaleRoutine(initValue.scale));
    }
    
    IEnumerator DistortionScaleRoutine(float destValue)
    {
        float t_Progression = 0f;
        float t_Duration = .55f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = jellyMat.GetFloat(DISTORTION_SCALE);
        float t_DestValue = destValue;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            float t_Value = Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
            jellyMat.SetFloat(DISTORTION_SCALE,t_Value);
 
            if (t_Progression >= 1f)
            {
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(DistortionScaleRoutine(initValue.scale));
    }
    IEnumerator DistortionRoutine(float destValue)
    {
        float t_Progression = 0f;
        float t_Duration = .65f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = jellyMat.GetFloat(DISTORTION);
        float t_DestValue = destValue;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            float t_Value = Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
            jellyMat.SetFloat(DISTORTION,t_Value);
 
            if (t_Progression >= 1f)
            {
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(DistortionRoutine(initValue.distortion));
    }
    IEnumerator WobbleSpeedRoutine(float destValue)
    {
        float t_Progression = 0f;
        float t_Duration = .45f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = jellyMat.GetFloat(WOBBLE_SPEED);
        float t_DestValue = destValue;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            float t_Value = Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
            jellyMat.SetFloat(WOBBLE_SPEED,t_Value);
 
            if (t_Progression >= 1f)
            {
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(WobbleSpeedRoutine(initValue.speed));
    }
    
    IEnumerator JellyHeightRoutine(float destValue)
    {
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = jellyMat.GetFloat(JELLY_HEIGHT);
        float t_DestValue = destValue;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            float t_Value = Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
            jellyMat.SetFloat(JELLY_HEIGHT,t_Value);
 
            if (t_Progression >= 1f)
            {
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(JellyHeightRoutine(initValue.height));
    }

    
}
