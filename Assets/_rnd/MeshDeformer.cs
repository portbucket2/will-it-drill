﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshFilter))]
public class MeshDeformer : MonoBehaviour
{
    // code for smoothing
    [System.Serializable]
    enum FilterType
    {
        Laplacian, HC
    };

    MeshFilter filter;

    [SerializeField, Range(0f, 1f)] float intensity = 0.5f;
    [SerializeField] FilterType type;
    [SerializeField, Range(0, 20)] int times = 3;
    [SerializeField, Range(0f, 1f)] float hcAlpha = 0.5f;
    [SerializeField, Range(0f, 1f)] float hcBeta = 0.5f;
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    Mesh deformingMesh;
    Vector3[] originalVertices, displacedVertices;
    Vector3[] vertexVelocities;

    public float springForce = 20f;
    public float damping = 5f;
    public float impactRadius;

    public SphereCollider sphereCollider;
    public BoxCollider boxCollider;

    [Header("Shaky")] 
    public MeshRenderer meshRenderer;
    public Material regularJelly;
    public Material shakyMat;
    
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        sphereCollider = GetComponent<SphereCollider>();
       // boxCollider = GetComponent<BoxCollider>();
        
        filter = GetComponent<MeshFilter>();
        deformingMesh = GetComponent<MeshFilter>().mesh;
        
        originalVertices = deformingMesh.vertices; // get vertices from defined class
        displacedVertices = new Vector3[originalVertices.Length];
        for (int i = 0; i < originalVertices.Length; i++)
        {
            displacedVertices[i] = originalVertices[i];
        }
        vertexVelocities = new Vector3[originalVertices.Length];

        Debug.Log("Deformer Initialized !!!! ");

    }

    Vector3 vertext_orginal;
    private Vector3 distance_temp;
    private float distanceFromPoint;
    private float radius,sine;
    
    public void AddDeformingForce(Vector3 point, float force)
    {
        point = transform.InverseTransformPoint(point); // world to local space 
        Debug.DrawLine(Camera.main.transform.position, point);
//        Debug.Log("Line should appear !!!");
        
        for (int i=0 ; i< originalVertices.Length ; i++ )
        {
            vertext_orginal = originalVertices[i];
            distance_temp.x = vertext_orginal.x - point.x;
            distance_temp.y = vertext_orginal.y - point.y;
            distance_temp.z = vertext_orginal.z - point.z;
            distanceFromPoint = Mathf.Sqrt(distance_temp.x * distance_temp.x + distance_temp.y * distance_temp.y + distance_temp.z * distance_temp.z);

            if (distanceFromPoint <= impactRadius)
            {
               // radius = 1 - (distanceFromPoint / impactRadius);
               // sine = (Mathf.Sin((radius - 0.5f) * Mathf.PI) + 1)*0.5f;
                
                float attenuatedForce = force / (.5f+ distanceFromPoint);
                float velocity = attenuatedForce * Time.smoothDeltaTime;
                vertexVelocities[i] -= vertext_orginal.normalized * velocity;
            }
            else
            {
                vertexVelocities[i] = Vector3.zero;
            }
        }
    }
    void Update()
    {
        for (int i = 0; i < displacedVertices.Length; i++)
        {
            UpdateVertex(i);
        }
        deformingMesh.vertices = displacedVertices; // update Vertices of Mesh
        deformingMesh.RecalculateNormals();
        deformingMesh.RecalculateBounds();
        deformingMesh.RecalculateTangents();
        
        if (dis > 2)
        {
            dis = 0;
           // UpdateCollider();
        }
    }

    public float dis;

    private void UpdateVertex(int i)
    {
        float dt = Time.deltaTime;
        Vector3 velocity = vertexVelocities[i];
        Vector3 displacement = displacedVertices[i] - originalVertices[i];
        //velocity = velocity - displacement * springForce * dt;
        velocity = velocity - velocity * damping * dt;

        vertexVelocities[i] = velocity;
        displacedVertices[i] += velocity * dt;

        dis += displacement.magnitude;
    }

    public void UpdateCollider()
    {
       // sphereCollider.center -= new Vector3(0,0.0005f,0);
        //boxCollider.center -= new Vector3(0,0.0005f,0);
    }
    public void AddSmoothing()
    {
        switch (type)
        {
            case FilterType.Laplacian:
                //filter.mesh = MeshSmoothing.LaplacianFilter(deformingMesh, times);
                deformingMesh = MeshSmoothing.LaplacianFilter(deformingMesh, times);
                
                break;
            case FilterType.HC:
                //filter.mesh = MeshSmoothing.HCFilter(deformingMesh, times, hcAlpha, hcBeta);
                deformingMesh = MeshSmoothing.HCFilter(deformingMesh, times, hcAlpha, hcBeta);
                break;
        }
        displacedVertices = deformingMesh.vertices;
        Debug.Log("Smoothing Teeth activated !!!!!");
    }

    public void RegularMat()
    {
        meshRenderer.sharedMaterial = regularJelly;
    }

    public void ShakyMat()
    {
        meshRenderer.sharedMaterial = shakyMat;
    }
}