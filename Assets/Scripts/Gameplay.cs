using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;
    
    private void OnEnable()
    {
        Register();
    }

    private void OnDisable()
    {
        Unregister();
    }
    void Register()
    {
        TapAndHold.OnMouseButtonDownEvent += OnMouseDownEvent;
        TapAndHold.OnMouseButtonEvent += OnMouseButtonEvent;
        TapAndHold.OnMouseButtonUpEvent += OnMouseButtonUpEvent;

        choiceUiController.OnSendRegisteredListener += CurrentSelectedItem;
    }
    
    void Unregister()
    {
        TapAndHold.OnMouseButtonDownEvent -= OnMouseDownEvent;
        TapAndHold.OnMouseButtonEvent -= OnMouseButtonEvent;
        TapAndHold.OnMouseButtonUpEvent -= OnMouseButtonUpEvent;
        choiceUiController.OnSendRegisteredListener -= CurrentSelectedItem;
    }

    public bool isGameRunning;

    [Header("Gather Infos")] 
    public ChoiceUIController choiceUiController;

    public BitController bitController;
   
    [Header("Driller Infos")]
    public Transform drillerHolder;
    [Range(0f, 1f)] public float drillMoveRange;
    [Range(0f, 100f)] public float maxHoldThreshold;
    public float holdResetTime;
    public Vector3 initDrillerPos;
    public bool isHold;
    public float holdTime;

    public int currentSelectedID;
    
   
    
    private void Awake()
    {
        initDrillerPos = drillerHolder.position;
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
    }
    
    private void OnMouseDownEvent(float holdTime)
    {
        isHold = true;
    }
    
    private void OnMouseButtonEvent(float holdTime)
    {
        this.holdTime = holdTime;
        float t_Time = holdTime / maxHoldThreshold;
        
        if (isHold)
        {
            Vector3 destVec = new Vector3(initDrillerPos.x, initDrillerPos.y, initDrillerPos.z) - new Vector3(0,drillMoveRange,0);
            drillerHolder.position = Vector3.Lerp(drillerHolder.position, destVec, t_Time);
        }
    }
    
    private void OnMouseButtonUpEvent(float obj)
    {
        isHold = false;
        holdResetTime = holdTime;
        holdTime = 0;
    }
    
    void Update()
    {
        if (!isHold && holdResetTime > 0)
        {
            holdTime += Time.deltaTime;
            holdResetTime -= Time.deltaTime/16f;
            float t_Time = Mathf.Abs(holdTime / holdResetTime);
            drillerHolder.position = Vector3.Lerp(drillerHolder.position, initDrillerPos, t_Time);
        }
    }
    private void CurrentSelectedItem(int selectedID)
    {
        Debug.Log("SELECTED ID FROM GAMEPLAY========="+selectedID);
        currentSelectedID = selectedID;
        bitController.ActivateBit(selectedID);
    }
    public void StartGame()
    {
        Debug.Log("===========STARTING GAME ============");
        isGameRunning = true;
    }

    public void CheckGameResult(GameResult gameResult)
    {
        TapAndHold.Instance.isTakingInput = false;
        OnMouseButtonUpEvent(0);
        switch (gameResult)
        { 
            case GameResult.SUCCESS:
                UIManager.Instance.ShowLevelComplete();
                break;
            case GameResult.FAILED:
                UIManager.Instance.ShowLevelFailed();
                break;
            default:
                break;
        }
    }
    
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    public void GotoNextLevel()
    {
        if (LevelManager.Instance.GetCurrentLevel() >= 5)
        {
            PlayerPrefs.DeleteKey("Game_Level_Pref");
        }

        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        string loadScene = "Level" + t_CurrentLevel;

        if (SceneManager.GetActiveScene().name != loadScene)
        {
            SceneManager.LoadScene("Level" + t_CurrentLevel);
        }
    }

    public void DecreaseSpeed()
    {
        maxHoldThreshold += Time.deltaTime * 275;
        if (maxHoldThreshold > 1000)
        {
            maxHoldThreshold = 1000;
        }
    }

    public void IncreaseSpeed()
    {
        maxHoldThreshold = 60;
    }
    
//temp

   // public GameObject singleHolder;
   // public GameObject doubleHolder;
   // public MeshDeformer meshDeformer;
  //  public bool isDouble;

  //  public Transform fragTrans;
   // public List<GameObject> fragList;
  //  public void TestFragment()
  ////  {
   //     StartCoroutine(FragRoutine());
  //  }

  //  IEnumerator FragRoutine()
  //  {
       /* if (fragList.Count < 0)
        {
            for (int i = 0; i < fragTrans.childCount; i++)
            {
                fragList.Add(fragTrans.GetChild(i).gameObject.GetComponent<Rigidbody>());
            }
        }*/
       
       
      //  yield return new WaitForEndOfFrame();
        
      //  meshDeformer.gameObject.SetActive(false);
        
      //  for (int i = 0; i < fragList.Count; i++)
      //  {
       //     fragList[i].AddComponent<BoxCollider>();
       //     Rigidbody rig = fragList[i].AddComponent<Rigidbody>();
            //rig.useGravity = false;
       //     rig.AddForceAtPosition(Vector3.up, transform.position,ForceMode.Impulse);
      //  }
   // }

   

    //public void Single()
   // {
    //    isDouble = false;
    ////    singleHolder.SetActive(true);
    //    doubleHolder.SetActive(false);
    //    meshDeformer.impactRadius = 0.1f;
  //  }

 //   public void Double()
  //  {
   //     isDouble = true;
   //     doubleHolder.SetActive(true);
   //     singleHolder.SetActive(false);
   //     meshDeformer.impactRadius = 0.3f;
   // }
}
