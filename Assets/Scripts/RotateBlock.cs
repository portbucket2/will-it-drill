using UnityEngine;

public class RotateBlock : MonoBehaviour
{
    public Vector3 rotation;
    public float speed;
    
    private void Update()
    {
        transform.Rotate(rotation * speed * Time.deltaTime);
    }
}
