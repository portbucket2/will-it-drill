using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class BlockBase : MonoBehaviour
{
    //---- base class Modification---------/////
    public static event Action OnCollisionEnterEvent;
    public static event Action OnCollisionStayEvent;
    public static event Action OnCollisionExitEvent;
    
    public float currentDrillTime;
    [Range(0f,100f)] public float maxDrillTime;
    
    public GameResult gameResult;
    public UnityEvent<GameResult> OnGameResultEvent;
    
    public Transform blockHead;
    
    public float force = 10f ;
    public float forceOffset = 0.1f;

    public bool isDrilling;

    public bool isAddOffset;

    public ParticleSystem splashParticle;

    public LayerMask layerMask;

    public bool isUseParticle;
    
    public bool isUseJelly;

    public bool isUseDrillandJelly;
    
    public JellyController jellyController;

    [Header("EVENTS")] 
    public UnityEvent OnEnterEvent;
    public UnityEvent OnStayEvent;
    public UnityEvent OnExitEvent;
    
    SkinnedMeshRenderer meshRenderer;
    MeshCollider collider;

    public bool isExtraThings;

    private void Awake()
    {
        meshRenderer = GetComponent<SkinnedMeshRenderer>();
        collider = GetComponent<MeshCollider>();
    }

    private void Start()
    {
        
    }

    public void UpdateCollider()
    {
        if (meshRenderer != null)
        {
            Mesh colliderMesh = new Mesh();
            meshRenderer.BakeMesh(colliderMesh);
            if(collider!= null) collider.sharedMesh = null;
            if(collider!= null) collider.sharedMesh = colliderMesh;
        }
    }

    private void Update()
    {
        if (currentDrillTime > maxDrillTime)
        {
            currentDrillTime = 0;
            OnGameResultEvent.Invoke(gameResult);
        }
    }

    public void SendGameResultInstant()
    {
        currentDrillTime = maxDrillTime+1f;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("block"))
        {
            UpdateCollider();
            DrillFunction(other);
        }
    }
    private void OnCollisionStay(Collision other)
    {
        if (other.transform.CompareTag("block"))
        {
            UpdateCollider();
            DrillFunction(other);
        }
    }
    private void OnCollisionExit(Collision other)
    {
        if (other.collider.gameObject.layer == LayerMask.NameToLayer("block"))
        {
        }
    }

    public void Enter(Collision other)
    {
        OnEnterEvent?.Invoke();
        if (isUseParticle)
        {
            splashParticle.Play();
        }
    }

    public void Stay(Collision other)
    {
        OnStayEvent?.Invoke();
        if (isExtraThings)
        {
            currentDrillTime += Time.deltaTime;
        }
    }
    public void Exit(Collision other)
    {
        OnExitEvent?.Invoke();
        UpdateCollider();
        if (isUseParticle)
        {
            splashParticle.Stop(true);
            splashParticle.Clear();
        }

        isDrilling = false;
        if (isUseJelly)
        {
            switch (Gameplay.Instance.currentSelectedID)
            {
                case 0:
                    break;
                case 1:
                    jellyController.BananaExit();
                    break;
                case 2:
                    jellyController.BananaExit();
                    break;

                default:
                    break;
            }
        }
    }

    private Vector3 GetRandomOffset()
    {
        return new Vector3(Random.Range(0.01f, 0.1f), 0, Random.Range(0.01f, .1f));
    }

    private void DrillFunction(Collision other)
    {
        MeshDeformer deformer = other.transform.GetComponent<MeshDeformer>();
        Vector3 contactPoint =
            isAddOffset ? other.contacts[0].point + GetRandomOffset() : other.contacts[0].point;

        isDrilling = true;

        splashParticle.transform.position = contactPoint;

        if (deformer && isDrilling)
        {
            Vector3 point = contactPoint;
            point += contactPoint.normalized * forceOffset;
            deformer.AddDeformingForce(point, force);

            if (isUseDrillandJelly)
            {
                switch (Gameplay.Instance.currentSelectedID)
                {
                    case 0:
                        break;
                    case 1:
                        jellyController.BeaconStart();
                        break;
                    case 2:
                        jellyController.BananaStart();
                        break;

                    default:
                        break;
                }
            }
        }
        else
        {
            if (isUseJelly)
            {
                switch (Gameplay.Instance.currentSelectedID)
                {
                    case 0:
                        break;
                    case 1:
                        jellyController.BeaconStart();
                        break;
                    case 2:
                        jellyController.BananaStart();
                        break;

                    default:
                        break;
                }
            }
        }

        if (deformer)
        {
            deformer.UpdateCollider();
        }

        if (!isExtraThings)
        {
            currentDrillTime += Time.deltaTime;
        }
    }
}
