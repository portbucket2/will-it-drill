using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestroyObject : MonoBehaviour
{
   public BoxCollider boxCollider;

   public UnityEvent OnTriggEnter;
   
   
   private void Awake()
   {
      boxCollider = GetComponent<BoxCollider>();
   }

   private void OnTriggerEnter(Collider other)
   {
      if (other.transform.CompareTag("block"))
      {
         OnTriggEnter?.Invoke();
         Destroy(gameObject,0.3f);
      }
   }
}
