using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananaBit : BitBase
{
    public Rigidbody bananaBody;
    public List<Cloth> clothList;

    
    
    public void StartBananaThings()
    {
        if (enabled)
        {
            StartCoroutine(BananaRoutine());
        }
    }

    IEnumerator BananaRoutine()
    {
        yield return new WaitForSeconds(1f);
        bananaBody.transform.SetParent(null);
        bananaBody.useGravity = true;
        bananaBody.velocity = new Vector3(Random.Range(0,5),0,Random.Range(0,5));
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < clothList.Count; i++)
        {
            clothList[i].enabled = true;
        }
    }

    public override void SpecialFunction()
    {
        StartBananaThings();
    }
}
