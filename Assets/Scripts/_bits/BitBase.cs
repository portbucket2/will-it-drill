using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BitBase : MonoBehaviour
{
    public int bitID;
    public GameObject bitHolder;
    public GameObject blockObject;
    
    public Vector3 rotation;
    [Range(0f,100f)] public float speed;

    public bool isSpecialCall;
    
    private void Update()
    {
        bitHolder.transform.Rotate(rotation * speed * Time.deltaTime);
    }
    
    public void Activate()
    {
        if(bitHolder != null) bitHolder.SetActive(true);
       if(blockObject != null) blockObject.SetActive(true);

       if (isSpecialCall)
       {
           SpecialFunction();
       }
    }

    public void Deactivate()
    {
        if(bitHolder != null) bitHolder.SetActive(false);
        if(blockObject != null) blockObject.SetActive(false);
    }

    public abstract void SpecialFunction();
}
