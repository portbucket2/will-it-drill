using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LemonGrassBit : BitBase
{
    public List<Cloth> clothList;

    public void ActivateYVelocity()
    {
        for (int i = 0; i < clothList.Count; i++)
        {
            clothList[i].externalAcceleration = new Vector3(0,1000,0);
        }
    }

    public void DeactivateYVelocity()
    {
        for (int i = 0; i < clothList.Count; i++)
        {
            clothList[i].externalAcceleration = new Vector3(0,0,0);
        }
    }
    public override void SpecialFunction()
    {
        
    }
}
