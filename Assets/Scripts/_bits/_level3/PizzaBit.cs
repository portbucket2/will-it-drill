using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaBit : BitBase
{
    [Header("Pizza part")] public List<Transform> pepList;

    

    public override void SpecialFunction()
    {
        for (int i = 0; i < pepList.Count; i++)
        {
            pepList[i].SetParent(null);
        }
    }
}
