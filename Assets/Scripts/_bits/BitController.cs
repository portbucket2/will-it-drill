using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitController : MonoBehaviour
{
   public List<BitBase> bitList;

   public void ActivateBit(int itemID)
   {
      //DeactivateBit();
      for (int i = 0; i < bitList.Count; i++)
      {
         if (itemID == bitList[i].bitID)
         {
            bitList[i].Activate();
         }
      }
   }

   public void DeactivateBit()
   {
      //Deactivate All Bits
      for (int i = 0; i < bitList.Count; i++)
      {
         bitList[i].Deactivate();
      }
   }
   public void DeactivateBit(int itemID)
   {
      //Deactivate By ID
      for (int i = 0; i < bitList.Count; i++)
      {
         if (itemID == bitList[i].bitID)
         {
            bitList[i].Deactivate();
         }
      }
   }
   
}
