using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToastBit : BitBase
{
    public Transform centerPos;
    
    public List<Rigidbody> rigList;
    public BlockBase blockBase;

    public float touchedTime;
    public float maxTime;

    public bool isAlreadyExploied;
    
    public void AddExploision()
    {
        touchedTime += Time.deltaTime;
        if (touchedTime > maxTime)
        {
            if (!isAlreadyExploied)
            {
                isAlreadyExploied = true;
                StartCoroutine(ExploisionRoutine());
            }
        }
    }

    IEnumerator ExploisionRoutine()
    {
        blockBase.transform.GetComponent<MeshRenderer>().enabled = false;
        for (int i = 0; i < rigList.Count; i++)
        {
            rigList[i].useGravity = true;
            rigList[i].transform.SetParent(null);
            Vector3 dir =  centerPos.position - rigList[i].transform.position;
            rigList[i].AddForce(dir * 30,ForceMode.Impulse);
           
            yield return new WaitForSeconds(.1f);
        }
        
        yield return new WaitForSeconds(2f);
        blockBase.SendGameResultInstant();
    }

    public void ResetTime()
    {
        touchedTime = 0;
    }
    public override void SpecialFunction()
    {
        
    }
}
