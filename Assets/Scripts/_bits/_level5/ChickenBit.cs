using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenBit : BitBase
{
    public MeshRenderer meshRenderer;
    public List<Transform> legList;

    public Rigidbody brokenPart;

    public BlockBase legBlock;

    
    
    public BlockBase blockBase;

    public float touchedTime;
    public float maxTime;

    public bool isAlreadyExploied;
    public Transform centerPos;
    
    public void BrokeLeg()
    {
        brokenPart.transform.SetParent(null);
        brokenPart.useGravity = true;
        brokenPart.AddForce(new Vector3(Random.Range(-3f,3f),Random.Range(-3f,3f),Random.Range(-3f,3f)) * 5,ForceMode.Impulse);
        legBlock.SendGameResultInstant();
    }
    
    public void AddExploision()
    {
        touchedTime += Time.deltaTime;
        if (touchedTime > maxTime)
        {
            if (!isAlreadyExploied)
            {
                isAlreadyExploied = true;
                StartCoroutine(ExploisionRoutine());
            }
        }
    }

    IEnumerator ExploisionRoutine()
    {
        meshRenderer.enabled = false;
       
        for (int i = 0; i < legList.Count; i++)
        {
            legList[i].GetComponent<Rigidbody>().useGravity = true;
            legList[i].SetParent(null);
            Vector3 dir =  centerPos.position - legList[i].transform.position;
            legList[i].GetComponent<Rigidbody>().AddForce(dir + 
                                                          new Vector3(Random.Range(-3f,3f),Random.Range(-3f,3f),Random.Range(-3f,3f)) * 3,ForceMode.Impulse);
           
            yield return new WaitForEndOfFrame();
        }
        
       // yield return new WaitForSeconds(2f);
       // blockBase.SendGameResultInstant();
    }
    public void ResetTime()
    {
        touchedTime = 0;
    }
    
    
    public override void SpecialFunction()
    {
        
    }
}
