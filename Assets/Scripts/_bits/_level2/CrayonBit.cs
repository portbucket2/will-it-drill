using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrayonBit : BitBase
{
    [Header("Crayon parts")] public Transform point;

    public void DecreaseBoxCollider()
    {
        point.transform.localPosition += new Vector3(0,0,0.03f);
    }
    public override void SpecialFunction()
    {
        
    }
}
