using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkBit : BitBase
{
    [Header("Fork Part")] public GameObject orginalFork;
    public GameObject brokeFork;

    public void ActivateBrokenFork()
    {
        brokeFork.SetActive(true);
        orginalFork.SetActive(false);
    }
    public override void SpecialFunction()
    {
        
    }
}
