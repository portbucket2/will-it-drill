using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderDetect : MonoBehaviour
{
    public LayerMask layerMask;
    
    public UnityEvent<Collision> OnEnter;
    public UnityEvent<Collision> OnStay;
    public UnityEvent<Collision> OnExit;

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.gameObject.layer == LayerMask.NameToLayer("block"))
        {
            OnEnter.Invoke(other);
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.collider.gameObject.layer == LayerMask.NameToLayer("block"))
        {
            OnStay.Invoke(other);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.gameObject.layer == LayerMask.NameToLayer("block"))
        {
            OnExit.Invoke(other);
        }
    }
}
