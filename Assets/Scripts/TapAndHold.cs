using System;
using UnityEngine;

public class TapAndHold : MonoBehaviour
{
    public bool isTakingInput;
    public static TapAndHold Instance;
    public float holdingTime;

    public static event Action<float> OnMouseButtonDownEvent;
    public static event Action<float> OnMouseButtonEvent;
    public static event Action<float> OnMouseButtonUpEvent; 
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (isTakingInput)
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnMouseButtonDownEvent.Invoke(holdingTime);
            }

            if (Input.GetMouseButton(0))
            {
                holdingTime += Time.deltaTime;
                OnMouseButtonEvent.Invoke(holdingTime);
            }

            if (Input.GetMouseButtonUp(0))
            {
                ResetInput();
            }
        }
    }

    private void ResetInput()
    {
        OnMouseButtonUpEvent.Invoke(holdingTime);
        holdingTime = 0;
    }

    public void ActivateInput()
    {
        isTakingInput = true;
    }

    public void DeactivateInput()
    {
        isTakingInput = false;
    }
}
