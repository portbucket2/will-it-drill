using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    public TMP_InputField inputField;

    public static MenuScene Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("Scene - Test GM");
    }
}
