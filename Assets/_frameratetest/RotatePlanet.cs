using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlanet : MonoBehaviour
{
    public Vector3 rotation;
    public float speed;

    private void Update()
    {
        transform.Rotate(rotation * speed * Time.deltaTime);
    }
}
