using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestUIBack : MonoBehaviour
{
    public Planet planet;

    private void OnEnable()
    {
        if (MenuScene.Instance.inputField.text != "")
        {
            planet.resolution = Int32.Parse(MenuScene.Instance.inputField.text);
            planet.Initialize();
            planet.GenerateMesh();
        }
    }

    public void BackToMainScene()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
